package comp;

import flixel.FlxObject;

import comp.CComponent;
import mgr.CAnimationManager;

/**
 * ...
 * @author Dimitri
 */
class CAnimation extends CComponent
{
	
	public static var LEFT:String = 'left';
	public static var RIGHT:String = 'right';
	public static var UP:String = 'up';
	public static var DOWN:String = 'down';
	
	public static var WALK_LEFT:String = 'walk_left';
	public static var WALK_RIGHT:String = 'walk_right';
	public static var WALK_UP:String = 'walk_up';
	public static var WALK_DOWN:String = 'walk_down';	
	
	public static var JUMP:String = 'jump';
	
#if debug
	private var initOk:Array<String> = new Array<String>();
#end

	private var entity:CEntity;
	
	private var moveConfigured:Bool = false;
	private var jumpConfigured:Bool = false;
	
	public function new( entity:CEntity)
	{
		this.type = EComponentType.Animation;
		this.entity = entity;
		CAnimationManager.add( entity, this);
		
		super();
	}
	
	
#if debug
	public override function check()
	{
		if ( initOk.length == 0 || (!moveConfigured && !jumpConfigured) )
		{
			CDebug.error( 'No animations configured in CAnimation component' );
		}
	}
#end
	
	
	public function add( animationName:String, Frames:Array<Int>, ?FrameRate:Int = 30, ?Looped:Bool = true)
	{
#if debug
		initOk.push(animationName);
#end
		if (  LEFT == animationName || WALK_LEFT == animationName )
		{
			moveConfigured = true;
		} else if ( JUMP == animationName )
		{
			jumpConfigured = true;
		}
		entity.animation.add(animationName, Frames, FrameRate, Looped);
	}
	
	
	private inline function play( animationName:String)
	{
#if debug
		if ( !Lambda.has( initOk, animationName) )
		{
			CDebug.error('Animation not defined: $animationName');
		}
#end
		entity.animation.play(animationName);
	}
	

	public function update()
	{
		if( moveConfigured && entity.isMoving ) {
			switch(entity.facing)
			{
				case FlxObject.LEFT:
					play(WALK_LEFT);
				case FlxObject.RIGHT:
					play(WALK_RIGHT);
				case FlxObject.UP:
					play(WALK_UP);
				case FlxObject.DOWN:
					play(WALK_DOWN);
			} 
		}else {
			switch(entity.facing)
			{
				case FlxObject.LEFT:
					play(LEFT);
				case FlxObject.RIGHT:
					play(RIGHT);
				case FlxObject.UP:
					play(UP);
				case FlxObject.DOWN:
					play(DOWN);
			}
		}
	}	
	
}