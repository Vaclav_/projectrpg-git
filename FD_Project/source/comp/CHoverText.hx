package comp;

import flixel.text.FlxText;

import comp.CComponent;

/**
 * ...
 * @author Dimitri
 */
class CHoverText extends CComponent
{
	var entity:CEntity;
	
	var textObject:FlxText;
	var offsetX:Float;
	var offsetY:Float;
	
	public function new( entity:CEntity, offsetX:Int, offsetY:Int, text:FlxText ) 
	{
		this.type = EComponentType.HoverText;
		this.entity = entity;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.textObject = text;
		
		// TODO: add to manager
		
		// TODO: events: hover, click, fade in?
		// onHover( color, callback)
		// MouseEventManager.addSprite(new FlxSprite(), onMouseDown, onMouseUp, onMouseOver, onMouseOut);
		
		// TODO: attach to fixed point?
		
		super();
	}
	
	
	
}