package comp;

import flixel.input.keyboard.FlxKeyboard;
import flixel.FlxObject;
import flixel.util.FlxAngle;

import mgr.CInputManager;
import comp.CComponent;

/**
 * ...
 * @author Dimitri
 */
class CInput extends CComponent
{
	public var entity:CEntity;
	
	public var maxSpeed:Float = 0.0;
	public var inputCallback:FlxKeyboard->Float->Void = null;
	
	
	public function new( entity:CEntity) 
	{
		this.type = EComponentType.Input;
		this.entity = entity;
		CInputManager.add( entity, this);
		
		super();
	}
	

#if debug
	override public function check() 
	{
		if ( maxSpeed == 0.0 || inputCallback == null )
		{
			CDebug.error("Variable maxSpeed wasn't initialized");
		}
	}
#end
	
	public function playerInputHandler( keyboard:FlxKeyboard, dTime:Float ):Void {
		
        var up:Bool = false;
        var down:Bool = false;
        var left:Bool = false;
        var right:Bool = false;

        up = keyboard.anyPressed(["UP", "W"]);
        down = keyboard.anyPressed(["DOWN", "S"]);
        left = keyboard.anyPressed(["LEFT", "A"]);
        right = keyboard.anyPressed(["RIGHT", "D"]);
		
		if ( up || down || left || right )
		{
		   
			entity.isMoving = true;

            var mA:Float = 0;
            if (up)
            {
                mA = -90;
                if (left)
                    mA -= 45;
                else if (right)
                    mA += 45;

                entity.facing = FlxObject.UP;
            }
            else if (down)
            {
                mA = 90;
                if (left)
                    mA += 45;
                else if (right)
                    mA -= 45;

                entity.facing = FlxObject.DOWN;
            }
            else if (left) {
                mA = 180;
                entity.facing = FlxObject.LEFT;
            }
            else if (right) {
                mA = 0;
                entity.facing = FlxObject.RIGHT;
            }

            FlxAngle.rotatePoint( maxSpeed, 0, 0, 0, mA, entity.velocity);
		} else
		{
			entity.isMoving = false;
			// Stop
			entity.velocity.set(0.0);
		}
	}
	
}