package comp;

enum EComponentType {
	Input;
	Animation;
	HoverText;
	Collision;
}

/**
 * Base class of all components.
 * Contains data only, related to an Entity.
 * @author Dimitri
 */
class CComponent
{
	public var type:EComponentType;


	public function new() 
	{
#if debug
		CComponent.allComponents.push(this);
#end
	}


#if debug
	private static var allComponents:Array<CComponent> = new Array<CComponent>();
	
	public static function checkAll()
	{
		for ( c in allComponents )
		{
			c.check();
		}
	}

	public function check()
	{
	}
#end

}