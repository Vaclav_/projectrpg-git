package;

import flixel.FlxSprite;

import comp.CComponent;
import comp.CComponent.EComponentType;


/**
 * Every object that's visible is of this class. It contains common data, components and data per component.
 * Any specific functionality shall be added through components.
 * @author Dimitri
 */
class CEntity extends FlxSprite
{
	// Design decision:
	//  for a component accessed every frame, keep a reference
	//	for a component accessed occaisonally, use hash to look up each time
	public var components:Map<EComponentType, CComponent> = new Map<EComponentType, CComponent>();
	
	public var isMoving:Bool = false;
	
	public function new(X:Float=0, Y:Float=0, ?SimpleGraphic:Dynamic)
	{
		super(X, Y, SimpleGraphic);
		
	}
	
}