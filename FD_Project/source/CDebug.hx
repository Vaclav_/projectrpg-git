package;

import openfl.system.System;

/**
 * ...
 * @author Dimitri
 */
class CDebug
{
	public static var isOn:Bool = true;
	
	public function new() 
	{
	}
	
	public static function log( message:String)
	{
		if ( isOn )
		{
			trace(message);
		}
	}
	
	public static function error( message:String)
	{
		if ( isOn )
		{
			trace( "ERROR: " + message );
		#if desktop
			System.exit( -1);
		#end
		}
	}
	
}