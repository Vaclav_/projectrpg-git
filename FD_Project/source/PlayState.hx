package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;

import openfl.system.System;
import flixel.animation.FlxAnimation;

import comp.*;
import mgr.*;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class PlayState extends FlxState
{
	
	/* Entities */
	private var player:CEntity;
	
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{
		var t:FlxText = new FlxText("Hello, World!");
		t.text = 'THIS';
		add(t);
		
		player = new CEntity( 100, 200);
		player.loadGraphic(AssetPaths.blue_girl__png, true, 32, 48);
		this.add(player);
		
		var input:CInput = new CInput(player);
		input.maxSpeed = CPlayerInit.MAX_SPEED;
		input.inputCallback = input.playerInputHandler;
		
		var animation:CAnimation = new CAnimation(player);

		animation.add( CAnimation.LEFT, [0, 1, 2], 6, false);
        animation.add(CAnimation.WALK_DOWN, [0, 1, 2], 6, false);
        animation.add(CAnimation.DOWN, [1]);
        animation.add(CAnimation.WALK_LEFT, [3, 4, 5], 6, false);
        animation.add(CAnimation.LEFT, [4]);
        animation.add(CAnimation.WALK_RIGHT, [6, 7, 8], 6, false);
        animation.add(CAnimation.RIGHT, [7]);
        animation.add(CAnimation.WALK_UP, [9, 10, 11], 6, false);
        animation.add(CAnimation.UP, [10]);
		
#if debug
		CComponent.checkAll();
#end
	
		super.create();
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		CInputManager.update( FlxG.keys, FlxG.elapsed );
		CAnimationManager.update();
		
		super.update();
		
		if ( FlxG.keys.pressed.ESCAPE )
		{
			#if desktop
			System.exit(0);
			#end
		}
	}	
}