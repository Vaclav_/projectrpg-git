package mgr;

import comp.CAnimation;
import comp.CComponent;

/**
 * ...
 * @author Dimitri
 */
class CAnimationManager
{

	// private var entities:Array<CEntity> = new Array<CEntity>();
	private static var components:Array<CAnimation> = new Array<CAnimation>();

	public static function add( entity:CEntity, component:CAnimation ):Void
	{
		components.push( component );
		entity.components.set( EComponentType.Animation, component);
	}
	

	public static function update()
	{
		for ( c in components )
		{
			c.update();
		}
	}
	
}