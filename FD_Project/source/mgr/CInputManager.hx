package mgr;

import flixel.input.keyboard.FlxKeyboard;

import comp.CInput;
import comp.CComponent;


/**
 * ...
 * @author Dimitri
 */
class CInputManager
{

	// private static var entities:Array<CEntity> = new Array<CEntity>();
	private static var components:Array<CInput> = new Array<CInput>();
	
	public static function add( entity:CEntity, inputComponent:CInput ):Void
	{
		components.push( inputComponent );
		entity.components.set( EComponentType.Input, inputComponent);
	}
	
	
	public static function update( keyboard:FlxKeyboard, dTime:Float )
	{
		var i:Int = 0;
		for ( i in 0...components.length )
		{
			components[i].inputCallback( keyboard, dTime);
		}
	}
	
}